<?php

use App\Http\Middleware\CheckName;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Some changes
Route::get('/', function () {

    return view('welcome');
    //return redirect()->to('/admin/posts');
});

//hier wordt babyds.be/contact gerouted naar de view
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/geboortelijst', 'Admin\ReservationController@frontIndex');
Route::get('/geboortelijst/{id}', 'Admin\ReservationController@geboortelijst');
Route::get('/geboortelijst/reserve/{id}', 'Admin\ReservationController@frontShow');
Route::post('/geboortelijst/buy/{id}', 'Admin\ReservationController@buy');
Route::get('/geboortelijst/cancel/{str}', 'Admin\ReservationController@cancel');
Route::any('/geboortelijst/delete/{id}', 'Admin\ReservationController@delete');

Route::get('/admin', function () {
    return view('/admin/dashboard');
})->middleware('auth');

Route::get('/admin/categories', 'Admin\CategoriesController@index')->middleware('auth');

Route::get('/select_category', 'Admin\CategoriesController@select_category');


Route::get('/admin/links', 'Admin\LinksController@index')->middleware('auth');
Route::get('/admin/reservations', 'Admin\ReservationController@index')->middleware('auth');
Route::get('/admin/reservations/cancel/{id}', 'Admin\ReservationController@adminCancel')->middleware('auth');

Route::get('/log', function () {
    Log::info('This is an info log.');
    
    Log::warning('This is a warning.');

    Log::error('This is an error.');
});

Route::resource('admin/posts', 'Admin\\PostsController')->middleware('auth');
Route::get('admin/posts/{id}/editprijs', 'Admin\\PostsController@editprijs')->middleware('auth');


Route::resource('admin/categories', 'Admin\\CategoriesController')->middleware('auth');
Route::resource('admin/reservations', 'Admin\\ReservationController')->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('contacteren', 'HomeController@contact');

//for minecraft
Route::get('/map', 'Admin\CategoriesController@map');
