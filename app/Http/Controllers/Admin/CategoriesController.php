<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = \App\Categorie::all();
        return view('admin.categories.index', compact('categories'));
    }

     /**
     * Show the form for creating a new categorie.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created categorie in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $categorie_data = array(
            "categorie" => $requestData['categorie']
        );

        DB::table('categories')->insert($categorie_data);

        return redirect('/admin/categories')->with('flash_message', 'categorie added!');
    }

    /**
     * Show the form for editing the specified categorie.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $categorie_record = DB::table('categories')->where('id', '=', $id)->first();
        
        return view('admin.categories.edit', compact('categorie_record'));
    }

    /**
     * Update the specified categorie in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $categorie_data = array(
            "categorie" => $requestData['categorie']
        );
        
        DB::table('categories')->where('id', $id)->update($categorie_data);

        return redirect('/admin/categories')->with('flash_message', 'Post updated!');
    }

    /**
     * Remove the specified categorie from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DB::table('categories')->where('id',$id)->delete();

        return redirect('/admin/categories')->with('flash_message', 'Post deleted!');
    }

    /**
     * Display the specified categorie.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $categorie_record = DB::table('categories')->where('id', '=', $id)->first();
        return view('admin.categories.show', compact('categorie_record'));
    }

    public function select_category()
    {
        $categorie_record = DB::table('categories')->get();
        //echo "<pre>"; print_r($categorie_record); exit;
        return json_encode($categorie_record);
    }

    public function map()
    {
        return view('admin.categories.map');
    }
}
