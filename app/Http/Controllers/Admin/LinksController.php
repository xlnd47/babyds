<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use DB;

class LinksController extends Controller
{
    public function index(){
    	$links = DB::table('links')
	            ->join('posts', 'posts.id', '=', 'links.post_id')
	            ->get();
        return view('admin.links.index', compact('links'));
    }
}
