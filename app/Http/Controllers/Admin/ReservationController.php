<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\PostCreated;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use DB;

class ReservationController extends Controller
{
    /**
     * Display a listing of the Gifts.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $reservations  = DB::select('SELECT r.*, p.title FROM reservation r LEFT JOIN posts p ON p.id = r.product_id');
    
        return view('admin.reservations.index', compact('reservations'));
    }
    /**
     * Display a listing of the Gifts.
     *
     * @return \Illuminate\View\View
     */
    public function frontIndex()
    {
        $posts  = DB::select('SELECT p.* FROM posts p LEFT JOIN reservation r ON r.product_id = p.id WHERE r.id IS NULL');

        return view('geboortelijst', compact('posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function geboortelijst($id)
    {
        $category = DB::table('categories')->find($id);

        $posts  = DB::select('SELECT p.* FROM posts p LEFT JOIN reservation r ON r.product_id = p.id WHERE r.id IS NULL');

        return json_encode($posts);
    }

    /**
     * Display the specified product for gifting.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function frontShow($id)
    {
        $post   = Post::findOrFail($id);
        $links  = DB::table('links')->where('post_id', '=', $id)->get();
        return view('geboortelijstshow', compact('post','links'));
    }

    /**
     * Buy the specified product for gifting.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function buy(Request $request,$id)
    {
        $email      = $request->email;
        $reservations = array(
              "product_id" => $id,
              "email" => $email,
              "created_at" => date("Y/m/d"),
              "updated_at" => date("Y/m/d")
            );
           
        $rId        = DB::table('reservation')->insertGetId($reservations);
        $urlId      = base64_encode($rId);
        $cancelUrl  = url('/geboortelijst/cancel/'.$urlId);     
        //updating posts(product) table
        $posts = array(
          "updated_at" => date("Y/m/d")
        );
        $producten = DB::table('posts')->where('id',$id)->get();
        
        foreach ($producten as $product){
            $productnaam = $product->title;
        }
        DB::table('posts')->where('id',$id)->update($posts);
        $links = DB::table('links')->where('post_id',$id)->get();
        //email for sending links to buyer



        $toUser = $email;


        $subject = 'Je hebt een '.$productnaam.' gereserveerd!';
        $from = config('mail.from');


        Mail::send('emails.send', ['title' => $subject, 'cancel' => $cancelUrl, 'product' => $productnaam, 'links' => $links],function ($message) use($toUser, $subject) 
        {
            $message->from('no-reply@babyds.be', 'BabyDS');
            $message->to($toUser);
            $message->subject($subject);
        });

        return redirect('/geboortelijst/')->with('success', 'Top! Je hebt iets gereserveerd! We sturen je nu een mail, check zeker ook je spam.');
    }


    public function cancel($str)
    {
        if(!empty($str)){
            $id     = base64_decode($str);
            $resp   = DB::table('reservation')->where('id',$id)->delete();
            if($resp === false){
                return redirect('/geboortelijst')->with('error','Hmm.. we vonden deze reservatie niet, ofwel heb je deze al geannuleerd?');
            }else{
                return redirect('/geboortelijst')->with('success','Je reservatie is geannuleerd!');
            }
        }else{
            return redirect('/geboortelijst')->with('error','Whoops iets vreemds gebeurd, Axel wordt op de hoogte gebracht!');;
        }
    }

    public function adminCancel($id)
    {
        if(!empty($id)){
            $resp   = DB::table('reservation')->where('id',$id)->delete();
            if($resp === false){
                return redirect('/admin/reservations')->with('error','Oei das nie gelukt..');
            }else{
                return redirect('/admin/reservations')->with('success','Tis geannuleerd');
            }
        }else{
            return redirect('/admin/reservations')->with('error','Oeioei, daar ging wel iets mis');;
        }
    }
}
