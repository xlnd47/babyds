<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\PostCreated;
use App\Post;
use Illuminate\Http\Request;
use DB;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $posts = Post::where('title', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('category', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $posts = Post::latest()->paginate($perPage);
        }

        return view('admin.posts.index', compact('posts'));
    }

    
    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $newfilename = '';
        $imagePath = public_path('/images');
        if (isset($_FILES["profile_image"])) {
            $temp = explode(".", $_FILES["profile_image"]["name"]);
            $newfilename = time().'_'.$temp[0]. '.' . end($temp);
            move_uploaded_file($_FILES["profile_image"]["tmp_name"], $imagePath."/".$newfilename);            
        }

        $post_data = array(
            "title" => $requestData['title'],
            "prijs" => $requestData['prijs'],
            "content" => $requestData['content'],
            "category" => $requestData['category'],
            "image" => $newfilename
        );
        $insertid = Post::create($post_data);

        if(!empty($requestData['link'])) {
            foreach($requestData['link'] as $link) {
                $link_data = array(
                    "post_id" => $insertid->id,
                    "link" => $link
                );
                DB::table('links')->insert($link_data);
            }
        }
        dispatch(new PostCreated);
        return redirect('admin/posts')->with('flash_message', 'Post added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $links = DB::table('links')->where('post_id', '=', $id)->get();
        return view('admin.posts.show', compact('post','links'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $links = DB::table('links')->where('post_id', '=', $id)->get();
        return view('admin.posts.edit', compact('post','links'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function editprijs($id)
    {
        $post = Post::find($id);
        return view('admin.posts.editprijs', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        $newfilename = '';
        $imagePath = public_path('/images');
        if (isset($_FILES["profile_image"])) {
            $temp = explode(".", $_FILES["profile_image"]["name"]);
            $newfilename = time().'_'.$temp[0]. '.' . end($temp);
            move_uploaded_file($_FILES["profile_image"]["tmp_name"], $imagePath."/".$newfilename);      
            $requestData["image"] =  $newfilename;     
        }
        $post = Post::findOrFail($id);
        $post->update($requestData);

        DB::table('links')->where('post_id',$id)->delete();
        if(!empty($requestData['link'])) {
            foreach($requestData['link'] as $link) {
                if(!empty($link)){
                    $link_data = array(
                        "post_id" => $id,
                        "link" => $link
                    );
                    DB::table('links')->insert($link_data);
                }
            }
        }

        return redirect('admin/posts')->with('flash_message', 'Post updated!');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateprijs(Request $request, $id)
    {
        $request->validate([
            'prjs'=>required
        ]);

        $post = Post::find($id);
        $post->prijs = $request->get('prijs');
        $posts->save();

        return redirect('admin/posts')->with('flash_message','Prijs aangepast!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Post::destroy($id);
        DB::table('links')->where('post_id',$id)->delete();

        return redirect('admin/posts')->with('flash_message', 'Post deleted!');
    }
}
