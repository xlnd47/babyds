<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul class="nav list-group" role="tablist">
                <li role="presentation" class="list-group-item">
                    <a href="{{ url('/admin') }}">
                        Dashboard
                    </a>
                </li>
                <li role="presentation" class="list-group-item">
                    <a href="{{ url('/admin/posts') }}">
                        Geboortelijst
                    </a>
                </li>
                <li role="presentation" class="list-group-item">
                    <a href="{{ url('/admin/categories') }}">
                        Categorieën
                    </a>
                </li>
                <li role="presentation" class="list-group-item">
                    <a href="{{ url('/admin/links') }}">
                        Links
                    </a>
                </li>
                <li role="presentation" class="list-group-item">
                    <a href="{{ url('/admin/reservations') }}">
                        Reservations
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
