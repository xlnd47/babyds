<div class="form-group {{ $errors->has('categorie') ? 'has-error' : ''}}">
    <label for="categorie" class="col-md-4 control-label">{{ 'categorie' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="categorie" type="text" id="categorie" value="{{ $categorie_record->categorie or ''}}" >
        {!! $errors->first('categorie', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>