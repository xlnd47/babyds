<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="col-md-4 control-label">{{ 'Titel' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="title" type="text" id="title" value="{{ $post->title or ''}}" >
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="col-md-4 control-label">{{ 'Prijs' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="prijs" type="text" id="prijs" value="{{ $post->prijs or ''}}" >
        {!! $errors->first('prijs', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
    <label for="content" class="col-md-4 control-label">{{ 'Details' }}</label>
    <div class="col-md-6">
        <textarea class="form-control" rows="5" name="content" type="textarea" id="content" >{{ $post->content or ''}}</textarea>
        {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
    <label for="content" class="col-md-4 control-label">{{ 'Image' }}</label>
    <div class="col-md-6">
        <input id="profile_image" type="file" class="form-control" name="profile_image">
        {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <label for="link" class="col-md-4 control-label">{{ 'Link' }}</label>
    @if(isset($links))
        @foreach ($links as $link)
            @if(!empty($link->link))
            <div class="row dataRow">
                <div class="col-md-6"style="padding-left: 30px;">
                    <input class="form-control" name="link[]" type="text" id="link" value="{{ $link->link or ''}}" >
                    {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="col-md-2">
                    <div class="btnHolder btnDel">
                      <i class="fa fa-trash"></i>
                    </div>
                </div>
            </div>
            @endif
        @endforeach
    @endif
    <div class="row linkRow">
        <div class="col-md-6" style="padding-left: 30px;">
            <input class="form-control" name="link[]" type="text" id="link" value="" >
            {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
            <span id="response"></span>
        </div>
        <div class="col-md-2">
            <div class="btnHolder btnAdd">
              <i class="fa fa-plus"></i>
            </div>
        </div>
    </div>
</div>

<div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
    <label for="category" class="col-md-4 control-label">{{ 'Categorie' }}</label>
    <div class="col-md-6">
        <select name="category" class="form-control" id="category" >
            <!-- @foreach (json_decode('{"slapen": "Slapen", "in de auto": "In de auto", "op stap": "Op stap", "eten en drinken": "Eten en drinken", "spelen": "Spelen"}', true) as $optionKey => $optionValue) -->
                <option value="" <?php if(isset($post->category)) {echo "selected";} ?> ></option>
            <!-- @endforeach -->
        </select>
        {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    var html =  '<div class="row dataRow">\
    <div class="col-md-6">\
        <input class="form-control" name="link[]" type="text" id="link" value="" >\
        <span id="response"></span>\
    </div>\
    <div class="col-md-2">\
        <div class="btnHolder btnDel">\
          <i class="fa fa-trash"></i>\
        </div>\
    </div>\
    </div>';

    $('.btnAdd').click(function(){
        $(document).find('.linkRow:last').after(html);
    });
    $(document).on('click','.btnDel',function(){
        console.log('here');
        $(this).parents('.dataRow').remove();
    });
});

//for getting category for selection
var APP_URL = {!! json_encode(secure_url('/')) !!}
    $.ajax({
        url: APP_URL+'/select_category',
        dataType: 'json',
        data: '',
        success: function (data) {
            $("#category").empty();
            $.each(data, function (index, optiondata) {
                $("#category").append("<option value='" + optiondata.categorie + "'>" + optiondata.categorie + "</option>");
            });
        }
    });
</script>
</script>
