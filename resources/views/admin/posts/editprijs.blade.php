@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Prijs van "{{ $post->title }}" veranderen</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/posts') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="post" action="{{ route('admin.posts.updateprijs', $post->id)}}">
                            @method('PATCH')
                            @csrf
                            <div class ="form-group">
                                <label for="prijs">Prijs:</label>
                                <input type="text" class="form-control" name="prijs" value="{{$post->prijs}}"/>
                            </div>
                            <button type="submit" class="btn btn-primary">Prijs updaten</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
