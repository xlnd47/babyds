<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="prijs" class="col-md-4 control-label">{{ 'Prijs' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="prijs" type="text" id="prijs" value="{{ $post->prijs or ''}}" >
        {!! $errors->first('prijs', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>

