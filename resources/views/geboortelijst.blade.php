@extends('layouts.app')
<style>
.imgdiv {
  width: 200px;
  height: 200px;
}

.imgdiv img{
  height: 100%;
  width: 100%;
}
.beschrijving{
  height: 2.1em;
  overflow: hidden;
  line-height: 1em;
}
.btn {
    margin-top:15px;
    margin-bottom:15px;
}

.active {
    text-decoration: underline;
    text-decoration-color: #006824;
}
.header{
    padding : .75rem 1.25rem;
}
.list-item{
    position: relative;
    display: block;
    padding: .75rem 1.25rem;
    margin-bottom: -1px;
    background-color: #fff;
}
.img-thumbnail{
    border: none  !important;
}


</style>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="header">
                        Menu
                    </div>

                    <div class="card-body" id="lijst">
                        <ul class="nav list-group" role="tablist" id="menu">
                            <li role="presentation" class="list-item">
                                <a href="#" id ="alles">
                                    Alles
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-warning">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="card">
                    <div id="card-header" class="header">Geboortelijst</div>
                        <div class="card-body" id="itemlijst">
                            @php 
                            $i = 0;
                            @endphp
                                <div class="row">
                                    
                                    @foreach($posts as $post)
                                        @php 
                                        $i++;
                                        @endphp
                                        <div class="col-sm">
                                            @if(isset($post->image))
                                            <div class="imgdiv">
                                                <a href="{{URL::to('geboortelijst/reserve')}}/{{$post->id}}"><img src="{{URL::to('/images/')}}/{{$post->image}}" alt="{{$post->title}}" class="img-thumbnail img"></a>
                                            </div>
                                            @else
                                                <img src="https://rdironworks.com/wp-content/uploads/2017/12/dummy-200x200.png" alt="..." class="img-thumbnail">
                                            @endif
                                            <h2>{{$post->title}}</h2>
                                            <p><strong>{{$post->category}}</strong></p>
                                            <div class="beschrijving">
                                                <p style="font-size=50%">{{$post->content}}</p>
                                            </div>
                                            <!-- <button href="{{URL::to('geboortelijst/reserve')}}/{{$post->id}}" type="button" class="btn btn-primary">Details</button> -->
                                            <a class="btn btn-outline-primary" href="{{URL::to('geboortelijst/reserve')}}/{{$post->id}}">Details</a>
                                        </div>
                                        
                                        @if ($i % 3 == 0)
                                        </div>
                                        <div class="row">
                                        @endif
                                        
                                        
                                    @endforeach
                                    @if ($i % 3 !=0)
                                    </div>
                                    @endif                               
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
    ophalenLijst();
    var text = "ok";
    var categorie;

    $(document).on("click", "a", function(){
        if (categorie){
            categorie.button('toggle');
        }
        categorie = $(this);
        $(this).button('toggle');  
        if (categorie.attr('id')){
            lijstLaden(categorie);
        }
     });   
     



});

//for getting category for selection
var APP_URL = {!! json_encode(secure_url('/')) !!}
    $.ajax({
        url: APP_URL+'/select_category',
        dataType: 'json',
        data: '',
        success: function (data) {
            $.each(data, function (index, optiondata) {
                $("#lijst ul").append('<li role="presentation" class="list-item"><a class="link" href="#" id="'+optiondata.categorie+'">' + optiondata.categorie +'</a></li>');
            });
        }
    });

    var tmp = null;

function ophalenLijst(){
        $.ajax({
        url: '/geboortelijst/1',
        dataType: 'json',
        data: '',
        success: function(data){
            tmp = data;
        }
    });
}


function lijstLaden(categorie){
        $("#itemlijst").empty();
        var lijst = '<div class="row">';
        var i = 0;
        ophalenLijst();
        
        $.each(tmp, function(index, value){
            if (categorie.attr('id') == "alles"){
                i++;
                lijst += '<div class="col-sm"><div class="imgdiv"><a href="https://babyds.be/geboortelijst/reserve/';
                lijst += value.id;
                lijst += '"><img src="https://babyds.be/images/';
                lijst += value.image;
                lijst += '" alt="' + value.title + '" class="img-thumbnail img"></a></div><h2>';
                lijst += value.title + '</h2><p><strong>' + value.category + '</strong></p><div class="beschrijving"><p style="font-size=50%">';
                lijst += value.content + '</p></div><a class="btn btn-outline-primary" href="https://babyds.be/geboortelijst/reserve/';
                lijst += value.id + '">Details</a></div>';

                if(i % 3 == 0){
                    lijst += '</div><div class="row">'
                }
            }else if (value.category == categorie.attr('id')){
                i++;
                lijst += '<div class="col-sm"><div class="imgdiv"><a href="https://babyds.be/geboortelijst/reserve/';
                lijst += value.id;
                lijst += '"><img src="https://babyds.be/images/';
                lijst += value.image;
                lijst += '" alt="' + value.title + '" class="img-thumbnail img"></a></div><h2>';
                lijst += value.title + '</h2><p><strong>' + value.category + '</strong></p><div class="beschrijving"><p style="font-size=50%">';
                lijst += value.content + '</p></div><a class="btn btn-outline-primary" href="https://babyds.be/geboortelijst/reserve/';
                lijst += value.id + '">Details</a></div>';

                if(i % 3 == 0){
                    lijst += '</div><div class="row">'
                }
            };
        });

        if (i % 3 != 0 ){
            lijst += '</div>';
        };

        $("#itemlijst").append(lijst);
    
}

</script>


@endsection
