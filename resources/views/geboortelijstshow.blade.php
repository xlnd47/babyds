@extends('layouts.app')
<style>
table td{
  border:none !important;
}

</style>

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                     @if(\Session::has('success'))
                        <div class="alert alert-success">
                        {{\Session::get('success')}}
                        </div>
                    @endif
                    <div class="card-header">{{ $post->title }}</div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                      <td><a class="btn btn-outline-primary" href="https://babyds.be/geboortelijst" role="button">Terug</a></td>
                                    </tr>
                                    <tr>
                                        <div class="auto-resize-square">
                                        <td> <img src="{{URL::to('/images/')}}/{{$post->image}}" class="img-thumbnail" style="width: 400px"> </td>
                                        </div>
                                    </tr>
                                    <tr>
                                        <td> {{ $post->content }} </td>
                                    </tr>
                                    <tr>
                                      <td>€{{ $post->prijs }}</td>
                                  </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- Modal -->
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myModal">Reserveren</button>
                            <div id="myModal" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  </div>
                                  <div class="modal-body">
                                    <form method="POST" action="{{ url('/geboortelijst/buy/' . $post->id) }}" accept-charset="UTF-8" style="display:inline">
                                        {{ csrf_field() }}
                                        <p>We sturen je de link(s) waar je een {{$post->title}} kan kopen</p>
                                        <label>Jouw e-mailadres</label>
                                        <input type="text" name="email" id='email'>
                                        <button class="btn btn-outline-primary" id="validateEmail" type="submit" title="Buy Post">Versturen</button>
                                    </form>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">sluiten</button>
                                  </div>
                                </div>

                              </div>
                            </div>
                            <!-- -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
  var $result = $("#result");
  var email = $("#email").val();
  $result.text("");

  if (validateEmail(email)) {
    $result.text(email + " is valid :)");
    $result.css("color", "green");
  } else {
    $result.text(email + " is not valid :(");
    $result.css("color", "red");
  }
  return false;
}

$("#validateEmail").on("click", validate);
</script>

